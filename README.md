## Installation
Install python3-pip, gevent, gunicorn, flask


## Running 
gunicorn --timeout 50 --worker-class gevent --workers 1 --threads 2 --worker-connections 1000 --bind 0.0.0.0:80 wsgi:app"
