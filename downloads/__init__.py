import requests
from flask import Flask

app = Flask(__name__)
options = {
    "address": "/latest/",
    "redirectCode": 302
}
    
def file_info():
    source = "https://gitlab.manjaro.org/webpage/iso-info/-/raw/master/file-info.json"
    response = requests.get(source)
    return response.json() 
    
from downloads.official import xfce
from downloads.official import gnome
from downloads.official import architect
from downloads.official import plasma

from downloads.community import awesome
from downloads.community import bspwm
from downloads.community import budgie
from downloads.community import cinnamon
from downloads.community import i3
from downloads.community import lxde
from downloads.community import lxqt
from downloads.community import mate
from downloads.community import openbox

