from flask import Flask, redirect
from downloads import app, options, file_info

# budgie
@app.route(f"{options['address']}budgie/image")
def budgie():
    return redirect(file_info()["budgie"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}budgie/checksum")
def budgie_checksum():
    return redirect(file_info()["budgie"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}budgie/signature")
def budgie_signature():
    return redirect(file_info()["budgie"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}budgie/torrent")
def budgie_torrent():
    return redirect(file_info()["budgie"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}budgie/minimal/image")
def budgie_minimal():
    return redirect(file_info()["budgie"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}budgie/minimal/checksum")
def budgie__minimal_checksum():
    return redirect(file_info()["budgie"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}budgie/minimal/signature")
def budgie_minimal_signature():
    return redirect(file_info()["budgie"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}budgie/minimal/torrent")
def budgie_minimal_torrent():
    return redirect(file_info()["budgie"]["minimal"]["torrent"], options["redirectCode"])
# budgie END  
