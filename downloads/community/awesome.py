from flask import Flask, redirect
from downloads import app, options, file_info

# awesome
@app.route(f"{options['address']}awesome/image")
def awesome():
    return redirect(file_info()["awesome"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}awesome/checksum")
def awesome_checksum():
    return redirect(file_info()["awesome"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}awesome/signature")
def awesome_signature():
    return redirect(file_info()["awesome"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}awesome/torrent")
def awesome_torrent():
    return redirect(file_info()["awesome"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}awesome/minimal/image")
def awesome_minimal():
    return redirect(file_info()["awesome"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}awesome/minimal/checksum")
def awesome__minimal_checksum():
    return redirect(file_info()["awesome"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}awesome/minimal/signature")
def awesome_minimal_signature():
    return redirect(file_info()["awesome"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}awesome/minimal/torrent")
def awesome_minimal_torrent():
    return redirect(file_info()["awesome"]["minimal"]["torrent"], options["redirectCode"])
# awesome END  
