from flask import Flask, redirect
from downloads import app, options, file_info

# i3
@app.route(f"{options['address']}i3/image")
def i3():
    return redirect(file_info()["i3"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}i3/checksum")
def i3_checksum():
    return redirect(file_info()["i3"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}i3/signature")
def i3_signature():
    return redirect(file_info()["i3"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}i3/torrent")
def i3_torrent():
    return redirect(file_info()["i3"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}i3/minimal/image")
def i3_minimal():
    return redirect(file_info()["i3"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}i3/minimal/checksum")
def i3__minimal_checksum():
    return redirect(file_info()["i3"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}i3/minimal/signature")
def i3_minimal_signature():
    return redirect(file_info()["i3"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}i3/minimal/torrent")
def i3_minimal_torrent():
    return redirect(file_info()["i3"]["minimal"]["torrent"], options["redirectCode"])
# i3 END  
