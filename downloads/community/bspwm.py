from flask import Flask, redirect
from downloads import app, options, file_info

# bspwm
@app.route(f"{options['address']}bspwm/image")
def bspwm():
    return redirect(file_info()["bspwm"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}bspwm/checksum")
def bspwm_checksum():
    return redirect(file_info()["bspwm"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}bspwm/signature")
def bspwm_signature():
    return redirect(file_info()["bspwm"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}bspwm/torrent")
def bspwm_torrent():
    return redirect(file_info()["bspwm"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}bspwm/minimal/image")
def bspwm_minimal():
    return redirect(file_info()["bspwm"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}bspwm/minimal/checksum")
def bspwm__minimal_checksum():
    return redirect(file_info()["bspwm"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}bspwm/minimal/signature")
def bspwm_minimal_signature():
    return redirect(file_info()["bspwm"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}bspwm/minimal/torrent")
def bspwm_minimal_torrent():
    return redirect(file_info()["bspwm"]["minimal"]["torrent"], options["redirectCode"])
# bspwm END  
