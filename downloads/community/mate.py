from flask import Flask, redirect
from downloads import app, options, file_info

# mate
@app.route(f"{options['address']}mate/image")
def mate():
    return redirect(file_info()["mate"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}mate/checksum")
def mate_checksum():
    return redirect(file_info()["mate"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}mate/signature")
def mate_signature():
    return redirect(file_info()["mate"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}mate/torrent")
def mate_torrent():
    return redirect(file_info()["mate"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}mate/minimal/image")
def mate_minimal():
    return redirect(file_info()["mate"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}mate/minimal/checksum")
def mate__minimal_checksum():
    return redirect(file_info()["mate"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}mate/minimal/signature")
def mate_minimal_signature():
    return redirect(file_info()["mate"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}mate/minimal/torrent")
def mate_minimal_torrent():
    return redirect(file_info()["mate"]["minimal"]["torrent"], options["redirectCode"])
# mate END  
