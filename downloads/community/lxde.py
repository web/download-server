from flask import Flask, redirect
from downloads import app, options, file_info

# lxde
@app.route(f"{options['address']}lxde/image")
def lxde():
    return redirect(file_info()["lxde"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}lxde/checksum")
def lxde_checksum():
    return redirect(file_info()["lxde"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}lxde/signature")
def lxde_signature():
    return redirect(file_info()["lxde"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}lxde/torrent")
def lxde_torrent():
    return redirect(file_info()["lxde"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}lxde/minimal/image")
def lxde_minimal():
    return redirect(file_info()["lxde"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}lxde/minimal/checksum")
def lxde__minimal_checksum():
    return redirect(file_info()["lxde"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}lxde/minimal/signature")
def lxde_minimal_signature():
    return redirect(file_info()["lxde"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}lxde/minimal/torrent")
def lxde_minimal_torrent():
    return redirect(file_info()["lxde"]["minimal"]["torrent"], options["redirectCode"])
# lxde END  
