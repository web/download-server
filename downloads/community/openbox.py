from flask import Flask, redirect
from downloads import app, options, file_info

# openbox
@app.route(f"{options['address']}openbox/image")
def openbox():
    return redirect(file_info()["openbox"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}openbox/checksum")
def openbox_checksum():
    return redirect(file_info()["openbox"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}openbox/signature")
def openbox_signature():
    return redirect(file_info()["openbox"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}openbox/torrent")
def openbox_torrent():
    return redirect(file_info()["openbox"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}openbox/minimal/image")
def openbox_minimal():
    return redirect(file_info()["openbox"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}openbox/minimal/checksum")
def openbox__minimal_checksum():
    return redirect(file_info()["openbox"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}openbox/minimal/signature")
def openbox_minimal_signature():
    return redirect(file_info()["openbox"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}openbox/minimal/torrent")
def openbox_minimal_torrent():
    return redirect(file_info()["openbox"]["minimal"]["torrent"], options["redirectCode"])
# openbox END  
