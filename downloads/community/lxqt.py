from flask import Flask, redirect
from downloads import app, options, file_info

# lxqt
@app.route(f"{options['address']}lxqt/image")
def lxqt():
    return redirect(file_info()["lxqt"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}lxqt/checksum")
def lxqt_checksum():
    return redirect(file_info()["lxqt"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}lxqt/signature")
def lxqt_signature():
    return redirect(file_info()["lxqt"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}lxqt/torrent")
def lxqt_torrent():
    return redirect(file_info()["lxqt"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}lxqt/minimal/image")
def lxqt_minimal():
    return redirect(file_info()["lxqt"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}lxqt/minimal/checksum")
def lxqt__minimal_checksum():
    return redirect(file_info()["lxqt"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}lxqt/minimal/signature")
def lxqt_minimal_signature():
    return redirect(file_info()["lxqt"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}lxqt/minimal/torrent")
def lxqt_minimal_torrent():
    return redirect(file_info()["lxqt"]["minimal"]["torrent"], options["redirectCode"])
# lxqt END  
