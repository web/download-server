from flask import Flask, redirect
from downloads import app, options, file_info

# cinnamon
@app.route(f"{options['address']}cinnamon/image")
def cinnamon():
    return redirect(file_info()["cinnamon"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}cinnamon/checksum")
def cinnamon_checksum():
    return redirect(file_info()["cinnamon"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}cinnamon/signature")
def cinnamon_signature():
    return redirect(file_info()["cinnamon"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}cinnamon/torrent")
def cinnamon_torrent():
    return redirect(file_info()["cinnamon"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}cinnamon/minimal/image")
def cinnamon_minimal():
    return redirect(file_info()["cinnamon"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}cinnamon/minimal/checksum")
def cinnamon__minimal_checksum():
    return redirect(file_info()["cinnamon"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}cinnamon/minimal/signature")
def cinnamon_minimal_signature():
    return redirect(file_info()["cinnamon"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}cinnamon/minimal/torrent")
def cinnamon_minimal_torrent():
    return redirect(file_info()["cinnamon"]["minimal"]["torrent"], options["redirectCode"])
# cinnamon END  
