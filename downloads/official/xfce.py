from flask import Flask, redirect
from downloads import app, options, file_info

# XFCE
@app.route(f"{options['address']}xfce/image")
def xfce():
    return redirect(file_info()["xfce"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}xfce/checksum")
def xfce_checksum():
    return redirect(file_info()["xfce"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}xfce/signature")
def xfce_signature():
    return redirect(file_info()["xfce"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}xfce/torrent")
def xfce_torrent():
    return redirect(file_info()["xfce"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}xfce/minimal/image")
def xfce_minimal():
    return redirect(file_info()["xfce"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}xfce/minimal/checksum")
def xfce__minimal_checksum():
    return redirect(file_info()["xfce"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}xfce/minimal/signature")
def xfce_minimal_signature():
    return redirect(file_info()["xfce"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}xfce/minimal/torrent")
def xfce_minimal_torrent():
    return redirect(file_info()["xfce"]["minimal"]["torrent"], options["redirectCode"])
# XFCE END  
