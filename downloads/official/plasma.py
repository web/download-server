from flask import Flask, redirect
from downloads import app, options, file_info

# plasma
@app.route(f"{options['address']}plasma/image")
def plasma():
    return redirect(file_info()["plasma"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}plasma/checksum")
def plasma_checksum():
    return redirect(file_info()["plasma"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}plasma/signature")
def plasma_signature():
    return redirect(file_info()["plasma"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}plasma/torrent")
def plasma_torrent():
    return redirect(file_info()["plasma"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}plasma/minimal")
def plasma_minimal():
    return redirect(file_info()["plasma"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}plasma/minimal/checksum")
def plasma__minimal_checksum():
    return redirect(file_info()["plasma"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}plasma/minimal/signature")
def plasma_minimal_signature():
    return redirect(file_info()["plasma"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}plasma/minimal/torrent")
def plasma_minimal_torrent():
    return redirect(file_info()["plasma"]["minimal"]["torrent"], options["redirectCode"])
# plasma END  
