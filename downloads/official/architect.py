from flask import Flask, redirect
from downloads import app, options, file_info

# architect
@app.route(f"{options['address']}architect/image")
def architect():
    return redirect(file_info()["architect"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}architect/checksum")
def architect_checksum():
    return redirect(file_info()["architect"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}architect/signature")
def architect_signature():
    return redirect(file_info()["architect"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}architect/torrent")
def architect_torrent():
    return redirect(file_info()["architect"]["torrent"], options["redirectCode"])
    
# architect END  
