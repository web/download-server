from flask import Flask, redirect
from downloads import app, options, file_info

# gnome
@app.route(f"{options['address']}gnome/image")
def gnome():
    return redirect(file_info()["gnome"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}gnome/checksum")
def gnome_checksum():
    return redirect(file_info()["gnome"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}gnome/signature")
def gnome_signature():
    return redirect(file_info()["gnome"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}gnome/torrent")
def gnome_torrent():
    return redirect(file_info()["gnome"]["torrent"], options["redirectCode"])
    
@app.route(f"{options['address']}gnome/minimal")
def gnome_minimal():
    return redirect(file_info()["gnome"]["minimal"]["image"], options["redirectCode"])
    
@app.route(f"{options['address']}gnome/minimal/checksum")
def gnome__minimal_checksum():
    return redirect(file_info()["gnome"]["minimal"]["checksum"], options["redirectCode"])
    
@app.route(f"{options['address']}gnome/minimal/signature")
def gnome_minimal_signature():
    return redirect(file_info()["gnome"]["minimal"]["signature"], options["redirectCode"])

@app.route(f"{options['address']}gnome/minimal/torrent")
def gnome_minimal_torrent():
    return redirect(file_info()["gnome"]["minimal"]["torrent"], options["redirectCode"])
# gnome END  
